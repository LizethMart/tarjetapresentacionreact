import Encabezado from '../src/Components/Encabezado'
import Body from '../src/Components/Body'
import Pie from '../src/Components/Pie'

function App() {


  return (
    <div className="App">
      <Encabezado/>
      <br/><br/>
      <Body/>
      <br/><br/>
      <Pie/>

    </div>
  )
}

export default App
