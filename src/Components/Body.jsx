import React from 'react'
import img1 from '../assets/2.jpeg'

export default function Body() {
  return (
    <>
        <div className="container">
            <br/>

            <div className="imagen">
                <img src={img1} width="150" height="200" />
            </div>

            <div className="titulo">
                <p>Lizeth Martinez Rosas</p>
            </div>

            <div className="cuadro2">
                <div className="texto2">
                    <h4>21 años</h4>
                </div>
            </div>

            <div className="cuadro3">
                <h4>Mexicana</h4>
            </div>

            <div className="cuadro4">
                <div className="texto3">
                    <h4>Sistemas<br/>computacionales</h4>
                </div>
            </div>

            <div className='cuadro5'>
                <div className='texto3'>
                    <h4>8vo Semestre</h4>
                </div>
            </div>

            <div className='cuadro6'>
                <div className='texto3'>
                    <h4>Jenni Lover</h4>
                </div>
            </div>

            <div className='cuadro7'>
                <div className='texto3'>
                    <h4>Escorpio</h4>
                </div>
            </div>

            <div className='con3'></div>
            <div className='con4'></div>
            <div className='con6'></div>
            <div className='con7'></div>
            <div className='con8'></div>

        </div>
    </>
  )
}
