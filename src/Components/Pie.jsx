import React from 'react'
import img3 from '../assets/3.png'
import img4 from '../assets/4.png'
import img5 from '../assets/5.png'
import img6 from '../assets/6.png'

export default function Pie() {
  return (
    <div className="contenedor">
    <a href="https://www.facebook.com/">
        <img src={img3} width="20" height="20"/>
    </a>
    <a href="https://www.instagram.com/">
        <img src={img4} width="20" height="20"/>
    </a>
    <a href="https://twitter.com/home?lang=es">
        <img src={img5} width="20" height="20" />
    </a>
    <a href="https://github.com/">
        <img src={img6} width="20" height="20"/>
    </a>
    </div>
  )
}
